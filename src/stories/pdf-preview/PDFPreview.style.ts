import { Theme } from "@material-ui/core/styles/createMuiTheme";
import createStyles from "@material-ui/core/styles/createStyles";

const styles = (theme: Theme) =>
  createStyles({
    container: {
      backgroundColor: "#FFFFFF",
      border: "1px solid #eee",
      borderRadius: 3,
      cursor: "pointer",
      fontSize: 15,
      margin: 10,
      padding: "3px 10px"
    }
  });

export default styles;
