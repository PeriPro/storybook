import { checkA11y } from "@storybook/addon-a11y";
import { storiesOf } from "@storybook/react";
import React from "react";
import PDFPreview from "./PDFPreview";
storiesOf("PDFPreview", module)
  .addDecorator(checkA11y)
  .add("with text", () => (
    <PDFPreview file={"example.pdf"}>Hello World</PDFPreview>
  ))
  .add("with some emoji", () => (
    <PDFPreview
      file={"example.pdf"}
      styles={{ backgroundColor: "red", color: "darkRed" }}
    >
      <span>😀 😎 👍 💯</span>
    </PDFPreview>
  ));
