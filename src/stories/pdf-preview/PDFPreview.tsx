import React from "react";
import { withStyles, WithStyles } from "@material-ui/core/styles";
import styles from "./PDFPreview.style";
import { Document, Page } from "react-pdf";

interface IPDFPreviewProps extends WithStyles<typeof styles> {
  children?: React.ReactNode;
  onClick?: (e: any) => void;
  styles?: {};
  file?: any;
}

function PDFPreview(props: IPDFPreviewProps) {
  return (
    <div className={props.classes.container}>
      {props.children}
      <Document file={props.file} noData={<h4>Please select a file</h4>}>
        <Page pageNumber={4} />
      </Document>
    </div>
  );
}

export default withStyles(styles)(PDFPreview);
