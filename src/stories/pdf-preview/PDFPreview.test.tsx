import React from "react";
import { shallow, mount } from "enzyme";
import PRFPreview from "./PDFPreview";

describe("Link", () => {
  it("Renders div to Google", () => {
    const div = shallow(
      <PRFPreview file={"example.pdf"}>Link to Google</PRFPreview>
    );
    expect(div).toMatchSnapshot();
  });

  it("Renders div to Google with classname", () => {
    const div = mount(
      <PRFPreview file={"example.pdf"}>Link to Google</PRFPreview>
    );
    //console.log(div.debug());
    //console.log(div.text());
    expect(div.text().includes("Link to Google")).toBe(true);
    expect(div).toMatchSnapshot();
  });
});
