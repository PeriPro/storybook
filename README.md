This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Storybook

[![pipeline status](https://gitlab.com/periplus/horus/badges/master/pipeline.svg)](https://gitlab.com/periplus/storybook/commits/master)
[![coverage report](https://gitlab.com/periplus/horus/badges/master/coverage.svg)](https://gitlab.com/periplus/storybook/commits/master)
[![Netlify Status](https://api.netlify.com/api/v1/badges/40a98694-c740-44c1-8c87-c4f301c26390/deploy-status)](https://app.netlify.com/sites/fervent-neumann-e4fecf/deploys)

No CI activated at the moment.

## Usage

`yarn run storybook`

http://localhost:9001

## Testing

- [Structural Testing: ](https://github.com/storybookjs/storybook/issues/4672)
  `yarn test`

  `To update the snapshots checkout the config: https://jestjs.io/docs/en/snapshot-testing`

- [Interaction Testing: ](https://airbnb.io/enzyme/)
  `yarn test`

  `Example: PDFPreview.test.tsx`

## Known issues

- [Missing babel-loader](https://github.com/storybookjs/storybook/issues/4672)
- [Configure your app for Jest](https://www.npmjs.com/package/@storybook/addon-storyshots#configure-jest-to-work-with-webpacks-requirecontext) (Only Option 2: Macro is working)
